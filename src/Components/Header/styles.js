import styled from 'styled-components';

export const Container = styled.div`
    width: 90%;
    max-width: 3600px;
    height: 100px;
    padding: 0 50px;
    display: flex;
    align-items: center;
    justify-content: flex-start;
`;