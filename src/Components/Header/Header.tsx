import React from 'react';
import { Logo } from '../Logo/Logo';
import { Container } from './styles';

export function Header() {
    return(
        <Container>
            <Logo type='small'/>
        </Container>
    );
} 