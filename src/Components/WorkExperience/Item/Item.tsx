import React, { FunctionComponent } from 'react';
import locationIcon from '../../../Statics/icons/location.png';
import { City, Company, Container, Date, Image, Info, LocationIcon } from './styles';

interface Props {
    company: string,
    image: string,
    startDate: string,
    endDate: string,
    city: string,
}

export const Item:FunctionComponent<Props> = ({company, image, startDate, endDate, city}) => {
    return(
        <Container>
            <Image src={image} alt={`${company} logo`}/>
            <Info>
                <Company>{company}</Company>
                <Date>{startDate} - {endDate}</Date>
                <City><LocationIcon alt="Location Icon" src={locationIcon}/>{city}</City>
            </Info>
        </Container>
    )
};