import styled from 'styled-components';

export const Image = styled.img`
    width: 60px;
    height: auto;
    border-radius: 100%;
    filter: grayscale(100%);
    &:hover{
        filter: grayscale(0%);
    }
    @media (max-width: 600px) {
        filter: grayscale(0%);
    }
`;

export const Container = styled.div`
    margin: 10px 30px;
    display: flex;
    align-items: center;
`;

export const Info = styled.div`
    padding: 0 20px;
`;

const text = `
    color: white;
`;

export const Company = styled.p`
    ${text};
    font-size: 16px;
    font-weight: 600;
`;

export const Date = styled.p`
    ${text};
    font-size: 14px;
    font-weight: 300;
`;

export const City = styled.p`
    display: flex;
    align-items: center;
    ${text};
    font-size: 14px;
    font-weight: 300;
`
export const LocationIcon = styled.img`
    width: 14px;
    margin-right: 3px;
`;