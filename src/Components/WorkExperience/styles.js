import styled from 'styled-components';

export const Container = styled.div`
    width: 100%;
    padding: 30px 0 0 0;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    @media (max-width: 600px) {
        justify-content: flex-start;
    }
`;