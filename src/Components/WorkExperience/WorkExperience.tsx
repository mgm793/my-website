import React, { FunctionComponent } from 'react';
import { WorkExperienceInfo } from '../../constants/DATA';
import { Item } from './Item/Item';
import { Container } from './styles';

export const WorkExperience:FunctionComponent = () => {
    return(
        <Container>
            {WorkExperienceInfo.map(work => (
                <Item 
                    key={work.company} 
                    company={work.company} 
                    image={work.image} 
                    startDate={work.start} 
                    endDate={work.end}
                    city={work.city}
                />
            )
        )}
        </Container>
    );
};