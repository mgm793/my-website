import React, { FunctionComponent, ReactNode } from 'react';
import { Container, Title } from './styles';

interface Props {
    title: string,
    children: ReactNode,
    isFullSize?: Boolean
}

export const Section:FunctionComponent<Props> = ({title, children, isFullSize}) => {
    return (
        <Container isFullSize={isFullSize}>
            <Title>{title}</Title>
            {children}
        </Container>
    );
}