import styled from 'styled-components';

const text = `
    color: white;
`;


export const Container = styled.div`
    width: ${props => props.isFullSize ? '100%' : '50%'};
    display: flex;
    flex-direction: column;
    padding: 50px 0;
    align-items: center;
    @media (max-width: 1100px) {
        width: 100%;
    }
`;

export const Title = styled.p`
    ${text};
    font-size: 25px;
    text-decoration: underline;
    font-weight: 600;
`;