import React, { FunctionComponent } from 'react';
import { Image, Container, Institution, Info, Title } from './styles';

interface Props {
    title: string,
    institution: string,
    image: string
}

export const Item:FunctionComponent<Props> = ({title, institution, image}) => {
    return(
        <Container>
            <Image src={image} alt={`${institution} logo`}/>
            <Info>
                <Institution>{institution}</Institution>
                <Title>{title}</Title>
            </Info>
        </Container>
    )
};