import React, { FunctionComponent } from 'react';
import { EducationInfo } from '../../constants/DATA';
import { Container } from './styles';
import { Item } from './Item/Item';


export const Education:FunctionComponent = () => {
    return(
        <Container>
            {EducationInfo.map(edu => <Item key={edu.title} institution={edu.institution} title={edu.title} image={edu.image}/>)}
        </Container>
    );
}