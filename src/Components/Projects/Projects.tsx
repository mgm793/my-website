import React, { FunctionComponent, ReactComponentElement } from 'react';
import { ProjectsInfo } from '../../constants/DATA';
import { Item } from './Item/Item';
import { Container } from './styles';

export const Projects:FunctionComponent = () => {
   
    return (
        <Container>
            {ProjectsInfo.map(project => <Item key={project.name} name={project.name} type={project.type} url={project.url} />)}
        </Container>
    );
} 