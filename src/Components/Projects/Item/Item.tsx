import React, { FunctionComponent, useEffect, useState } from 'react';
import { Container, DownloadIcon, Downloads, Logo, Name } from './styles';
import npmLogo from '../../../Statics/npm.png';
import downloadIcon from '../../../Statics/icons/download.png';
import vscodeLogo from '../../../Statics/vscode.png';


interface Props {
    name: string,
    type: string,
    url: string
}

interface Response {
    downloads: Number
}

async function getDownloads(name: string): Promise<Number>{
    let url = `https://api.npmjs.org/downloads/point/last-month/${name}`;
    const response : Response = await (await fetch(url)).json();
    return response.downloads;
}

function getIcon(type: string) : string {
    switch(type){
        case 'npm': 
            return npmLogo;
        case 'vsce':
            return vscodeLogo;
        default:
            return '';
    }
}

function goToProject(url: string): void{
    window.open(url);
}

export const Item:FunctionComponent<Props> = ({name, type, url}) => {
    const [downloads, setDownloads] = useState<Number>(-1);
    useEffect(() => {
        getDownloads(name).then((nDown) =>{
            setDownloads(nDown);
        });
    }, []);

    if(downloads === -1) return null;
    return (
        <Container onClick={() => goToProject(url) }>
            <Logo src={getIcon(type)} />
            <Name>{name}</Name>
            {type==='npm' && <Downloads><DownloadIcon src={downloadIcon}/>{downloads}</Downloads>}
        </Container>
    )
};