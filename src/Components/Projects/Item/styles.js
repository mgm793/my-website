import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    align-items: center;
    border: 1px solid white;
    border-radius: 4px;
    padding: 10px;
    margin: 0 20px;
    cursor: pointer;
    @media (max-width: 600px) {
        &:not(:last-child){
            margin-bottom: 10px;
        }
    }
`;

export const Name = styled.div`
    color: white;
`;

export const Downloads = styled.div`
    color: white;
    display: flex;
    align-items: center;
    margin-left: 20px;
`;

export const Logo = styled.img`
    width: 50px;
    margin-right: 20px;
`;

export const DownloadIcon = styled.img`
    width: 15px;
    margin-right: 5px;
`;