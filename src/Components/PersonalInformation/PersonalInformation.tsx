import React, { FunctionComponent } from 'react';
import { githubUrl, linkedinUrl, twitterUrl } from '../../constants/DATA';
import githubIcon from '../../Statics/icons/github.png';
import linkedinIcon from '../../Statics/icons/linkedin.png';
import locationIcon from '../../Statics/icons/location.png';
import twitterIcon from '../../Statics/icons/twitter.png';
import me from '../../Statics/me.jpeg';
import { Container, Image, Location, LocationIcon, Name, Social, SocialIcon, Status } from './styles';


export const PersonalInformation:FunctionComponent = () => {
    const redirect = (location : string) =>{
        window.open(location);
    }
    return (
        <Container>
            <Image src={me} alt="Marc's Photo"/>
            <Name>Marc Garcia i Mullon</Name>
            <Status>Software Engineer</Status>
            <Location>
                <LocationIcon src={locationIcon} alt='location logo'/>
                Oslo, Norway
            </Location>
            <Social>
                <SocialIcon src={linkedinIcon} onClick={redirect.bind(null, linkedinUrl)}/>
                <SocialIcon src={githubIcon} onClick={redirect.bind(null, githubUrl)}/>
                <SocialIcon src={twitterIcon} onClick={redirect.bind(null, twitterUrl)}/>
            </Social>
        </Container>
    );
} 