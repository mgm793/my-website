import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
`;

export const Image = styled.img`
    width: 200px;
    height: auto;
    border-radius: 100%;
    transition: filter 0.3s ease-in-out;
    filter: grayscale(100%);
    &:hover{
        filter: grayscale(0%); 
    }
    @media (max-width: 600px) {
        filter: grayscale(0%);
    }
`;

const text = `
    color: white;
`;

export const Name = styled.h1`
    ${text};
    padding: 20px 0 10px 0;
    font-weight: 600;
    @media (max-width: 600px) {
        font-size: 25px;
        text-align: center;
    }
`;

export const Status = styled.p`
    ${text};
    padding: 0 0 10px 0;
    font-size: 16px;
`;

export const Location = styled.div`
    ${text};
    display: flex;
    align-items: center;
    font-size: 13px;
    font-weight: 300;
`;

export const LocationIcon = styled.img`
    width: 15px;
    height: auto;
`;

export const Social = styled.div`
    display: flex;
    padding: 20px 0;
`;

export const SocialIcon = styled.img`
    width: 50px;
    height: 40px;
    padding: 0 5px;
    cursor: pointer;
`;