import React, { useState } from 'react';
import { LoadingScreen } from './Containers/LoadingScreen/LoadingScreen';
import { HomeScreen } from './Containers/HomeScreen/HomeScreen';

function App() {
  const [isLoaded, setIsLoaded] = useState(false);
  
  const handleClick = () : void => {
    setIsLoaded(true);
  };

	if (!isLoaded) {
		return <LoadingScreen handleClick={handleClick} />;
	}
	return <HomeScreen />;
}

export default App;
