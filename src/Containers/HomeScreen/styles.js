import styled from 'styled-components';

export const Container = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: flex-start;
`;

export const Content = styled.div`
    width: 90%;
    max-width: 3600px;
    min-height: 100%;
    padding: 50px;
    display: flex;
    flex-wrap: wrap;
    align-items: flex-start;
    @media (max-width: 600px) {
        width: 95%;
    }
`;