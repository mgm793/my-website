import React from 'react';
import { Container, Content } from './styles';
// import { Header } from '../../Components/Header/Header';
import { PersonalInformation } from '../../Components/PersonalInformation/PersonalInformation';
import { Section } from '../../Components/Section/Section';
import { WorkExperience } from '../../Components/WorkExperience/WorkExperience';
import { Education } from '../../Components/Education/Education';
import { Projects } from '../../Components/Projects/Projects';

function HomeScreen() {
	return (
		<Container>
			{/* <Header /> */}
			<Content>
				<PersonalInformation />
				<Section title='Work Experience'>
					<WorkExperience />
				</Section>
				<Section title='Education'>
					<Education />
				</Section>
				<Section title='Projects' isFullSize>
					<Projects />
				</Section>
			</Content>
		</Container>
	);
}

export { HomeScreen };
