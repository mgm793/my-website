import React from 'react';
import './styles.css';
import { Logo } from '../../Components/Logo/Logo';

interface Props {
	handleClick: Function;
}

function LoadingScreen(props: Props) {
	return (
		<div className='container'>
			<Logo />
			<div className='info' onClick={() => props.handleClick()}>
				start
			</div>
		</div>
	);
}

export { LoadingScreen };
